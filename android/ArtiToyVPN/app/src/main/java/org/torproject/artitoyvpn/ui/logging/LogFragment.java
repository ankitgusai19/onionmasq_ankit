package org.torproject.artitoyvpn.ui.logging;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.torproject.artitoyvpn.R;
import org.torproject.artitoyvpn.databinding.FragmentLogBinding;
import org.torproject.artitoyvpn.utils.Utils;

public class LogFragment extends Fragment {

    private FragmentLogBinding binding;

    private LogRecyclerViewAdapter recyclerViewAdapter;
    // duct tape st¥le
    private boolean showTimestamps = true;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        LogObservable logViewModel = LogObservable.getInstance();

        binding = FragmentLogBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final RecyclerView logList = binding.logList;

        LinearLayoutManager layoutManager = new LinearLayoutManager(container.getContext());
        logList.setLayoutManager(layoutManager);
        recyclerViewAdapter = new LogRecyclerViewAdapter();
        logList.setAdapter(recyclerViewAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(container.getContext(), layoutManager.getOrientation());
        logList.addItemDecoration(dividerItemDecoration);

        logViewModel.getLogListData().observe(getViewLifecycleOwner(), logItems -> recyclerViewAdapter.updateList(logItems));
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_logs, menu);
        menu.findItem(R.id.action_show_timestamps).setTitle(showTimestamps ?
                R.string.hide_timestamps : R.string.show_timestamps);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Context context = getContext();

        if (context == null) {
            return super.onOptionsItemSelected(item);
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_copy) {
            String logs = LogObservable.getInstance().getLogStrings(showTimestamps);
            Utils.writeTextToClipboard(context, logs);
            Toast.makeText(context.getApplicationContext(), getString(R.string.copied_to_clipboard), Toast.LENGTH_SHORT).show();
            return true;
        } else if (id == R.id.action_show_timestamps) {
            showTimestamps = !showTimestamps;
            recyclerViewAdapter.setShowTimeStamps(showTimestamps);
            item.setTitle(showTimestamps ? R.string.hide_timestamps : R.string.show_timestamps);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}